let registeredUsers = [
  'James Jeffries',
  'Gunther Smith',
  'Macie West',
  'Michelle Queen',
  'Shane Miguelito',
  'Fernando Dela Cruz',
  'Akiko Yukihime',
];

let friendsList = [];

function register(username) {
  if (registeredUsers.includes(username)) {
    alert('Registration failed. Username already exists!');
  } else {
    registeredUsers.push(username);
    alert('Thank you for registering!');
  }
}

console.log(registeredUsers);

function addFriend(username) {
  let foundUser = registeredUsers.find((user) => user === username);
  if (foundUser) {
    friendsList.push(foundUser);
    alert(`You have added ${foundUser} as a friend!`);
  } else {
    alert('User not found.');
  }
}

addFriend(register);
console.log(friendsList);

function displayFriends() {
  if (friendsList.length === 0) {
    alert('You currently have 0 friends. Add one first.');
  } else {
    friendsList.forEach((friend) => console.log(friend));
  }
}

displayFriends();

function displayNumberOfFriends() {
  if (friendsList.length === 0) {
    alert('You currently have 0 friends. Add one first.');
  } else {
    console.log(`You have ${friendsList.length} friends`);
  }
}

displayNumberOfFriends();

function deleteSpecificFriend(name) {
  if (friendsList.length === 0) {
    alert('You currently have 0 friends. Add one first.');
  } else {
    let index = friendsList.indexOf(name);
    if (index !== -1) {
      friendsList.splice(index, 1);
      alert(`${name} has been deleted from your friends list`);
    } else {
      alert('user not found');
    }
  }
}

deleteSpecificFriend();
console.log(friendsList);
